# Aurora Polynesie Francaise Changelog #

## Update 09 February 2025 ##
**{+ AIRAC : 2501 +}**

### Updated Files ###

- **FIXES files (fix)** : NTTT
- **VOR files (vor)** : NTTT
- **NDB files (ndb)** : NTTT
- **AWY files (hawy/lawy)** : NTTT
- **APT files (apt)** : NTTT
- **CTR files (lartcc)** : NTTR, NTAA, NTTM, NTTB
- **MSRA files (mva)** : NTTB, NTAA 
- **SID files (sid)** : NTTB, NTTE, NTTM
- **STAR files (str)** : NTAA, NTTG
- **VFR fixes files (vfi)** : NTAA, NTTB
- **Danger files (danger)** : NTTT
- **Restrict files (retrict)** : NTTT
- **Prohibit files (prohibit)** : NTTT
- **Colorscheme (clr)** : FRANCE

---
### Added Files ###

- **Symbols file (sym)** : FR

---
### Deleted Files ###

- **VFR Routes files (vrt)** : NTTR
- **Airspace Polygon files (tfl)** : NTTT_Radar

---
---

## Update 23 March 2024 ##
**{+ AIRAC : 2403 +}**

### Updated Files ###

- **APT files (apt)** : NTTT
- **RWY files (rwy)** : NTTT
- **FIXES files (fix)** : NTTT, NTTT_Oceanic
- **VOR files (vor)** : NTTT
- **NDB files (ndb)** : NTTT
- **AWY files (hawy/lawy)** : NTTT
- **ATC File (atc)** : AFIS, NTAA, NTTB, NTTM, NTTR, NTTT
- **CCR files (artcc)** : NTTT
- **TMA files (hartcc)** : NTAA
- **CTR files (lartcc)** : NTAA
- **MSRA files (mva)** : NTAA
- **SID files (sid)** : NTAA, NTTB, NTTH, NTTM, NTTP, NTTR
- **STAR files (str)** : NTAA, NTAR, NTAT, NTAV, NTGC, NTGF, NTGI, NTGJ, NTGM, NTGT, NTMD, NTMN, NTTB, NTTG, NTTH, NTTM, NTTO, NTTP, NTTR
- **Taxiway label files (txi)** : NTAA
- **VFR fixes files (vfi)** : NTAA, NTTR
- **VFR Routes files (vrt)** : NTTR <sup>X</sup>
- **GEO files (geo)** : NTTT
- **Prohibit files (prohibit)** : NTTT
- **Airspace Polygon files (tfl)** : NTTT_Radar <sup>X</sup>

---
### Added Files ###

- **ATIS file (atis)** : France
- **SID files (sid)** : NTTE
- **STAR files (str)** : NTAM, NTGA, NTGB, NTGE, NTGN, NTGV, NTGY, NTKO, NTKR, NTKU, NTTE
- **MSRA files (mva)** : NTMD, NTTB
- **Gates label files (gts)** : NTTB
- **VFR fixes files (vfi)** : NTTB, NTTM
- **Danger files (danger)** : NTTT
- **Restrict files (retrict)** : NTTT

<sup>X</sup> : File content has been withdrawn and file will be deleted in the next update.

---
---
## Update 31 January 2021 ##
**{+ AIRAC : 2101 +}**

**No Applicable Updates to date**

---
---
## Update 17 November 2020 ##
**{+ AIRAC : 2012 +}**

**No Applicable Updates to date**

---
---
## Update 10 October 2020 ##
**{+ AIRAC : 2011 +}**

### Updated Files ###

- **Prohibit file (prohibit)** : NTTT

---
---
## Update 19 September 2020 ##
**{+ AIRAC : 2010 +}**

### Updated Files ###

- **APT files (apt)** : NTTT
- **GEO files (geo)** : NTTT
- **ATC files (atc)** : AFIS

---
---
## Update 11 July 2020 ##
**{+ AIRAC : 2007 +}**

### Updated Files ###

- **TMA files (hartcc)** : NTAA
- **CCR files (artcc)** : NTTT

---
---
## Update 30 May 2020 ##
**{+ AIRAC : 2006 +}**

### Updated Files ###

- **STAR files (str)** : NTTR, NTTP, NTTO, NTTM, NTTH, NTTG, NTTB, NTMN, NTMD, NTGT, NTGM, NTGJ, NTGI, NTGF, NTGC, NTAV, NTAT, NTAR, NTAA
- **SID files (sid)** : NTTR, NTTP, NTTH, NTTB, NTAA
- **FIXES files (fix)** : NTTT - Addition of Oceanic Fixes in a seperate file (00N000W, 00S000W, etc) in hidden. Relevant fixes will be displayed if the "Route" function is used.

---
---
## Update 19 avril 2020 ##
**{+ AIRAC : 2004 +}**

### Updated Files ###

- **STAR files (str)** : NTTR, NTTP, NTTO, NTTM, NTTH, NTTG, NTTB, NTMN, NTMD, NTGT, NTGM, NTGJ, NTGI, NTGF, NTGC, NTAV, NTAT, NTAR, NTAA
- **SID files (sid)** : NTTR, NTTP, NTTH, NTTB, NTAA
- **FIXES files (fix)** : NTTT - Addition of Oceanic Fixes (00N000W, 00S000W, etc) in hidden. Relevant fixes will be displayed if the "Route" function is used.

---
### Added Files ###

- **VFR Routes file (vrt)** : NTTR